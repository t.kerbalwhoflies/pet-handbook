---
forceTheme: blue
---

# PET Handbook

## All Members

### (AM. 1) Don't touch core control's

PET may not touch any core controls that heat or cool the core. We are not security, and we are not there to melt or freeze the core. Read HT. 3-4.

### (AM. 2) No PBST Weapons on duty
PET may not carry any PBST weapons while on duty as PET, however gamepass gear such as OP Weapons are allowed.

### (AM. 3) No multiple ranking
PET Members may be in all teams if they wish, but a member in one team does not have authority over a member in another team. This still applies if one of the people is a high rank in the team, or if the person has the “Jack of All Trades” rank.

### (AM. 4) No double duty
PET may NOT be On - Duty in both PET and PBST at the same time, you will have to decide, and you may not be On - Duty for 2 teams at the same time.

### (AM. 5) No cross team conflicts
All teams are allowed to cooperate with one another, but there will be NO conflicts between teams. If a conflict does start, it is to be reported to a Team Chief, Manager, or the Director immediately.

### (AM. 6) Don't become mutent/set fire
PET are strictly forbidden to create fires or become mutants.

### (AM. 7) On Duty uniform
PET must wear their team’s uniform while On - Duty.

## Fire Team

### (FT. 1) Call on large fire
Fire Team are required to report any large fires using ``!call``.

### (FT. 2) On duty equipment
Fire Team are required to have a Fire Extinguisher and/or Fire Hose on them while On - Duty. They must also wear the fire suit.

### (FT. 3) Put out EVERY fire
Fire Team is required to put out any fires on sight, no matter what conditions.

### (FT. 4) Rescue anyone who needs it
Fire Team is also required to lead out visitors are any cost. Put their lives before yours.

## Medical Team

### (MT. 1) Always have a med kit
Medical Team is required to carry a med kit at all times.

### (MT. 2) Heal everyone (Exept enemies)
Medical Team are required to heal everyone they see whom are hurt, PBST members get healing priority. PET is not to heal enemies (ex. TMS, Raiders). 

### (MT. 3) No combat healing
Medical Team is not permitted to heal someone while they are in combat for 3 reasons.
#### (MT. 3A) No healing if dead
You cannot heal someone if you are dead. 
#### (MT. 3B) Unfair PVP
This makes it unfair for the one that person is fighting. 
#### (MT. 3C) May heal enemie
You may end up healing the one who you do not want to heal. 

## Hazmat Team

### (HT. 1) Wear suit at all times
Hazmat Team must always wear their hazmat suit at all times.

### (HT. 2) Be there for everyone
Hazmat Team must always put their lives at risk during disasters. They must always be there to save the facility.

### (HT. 3) Access to fans in disaster
Hazmat Team is permitted to use the fans during the neurotoxin disaster.

### (HT. 4) Only heat/cool core for save
Hazmat Team is permitted to use any of the heating systems in the core. However, this is only to save the core. Hazmat team is NOT allowed to cause meltdown or freezedown.

## Trainers and Chiefs

### (TC. 1) Have a good training schedule
Trainers and Chiefs must figure out their own times to host a trainings so that we don't collide with PBST, and they don't collide with us.

### (TC. 2) 1 Hours between PET trainings
Trainers and Chiefs have to follow the 1 Hour Training Policy, which means there cannot be a training 1 hours after a previous training. This only applies to PET trainings, PBST trainings do not comply with this rule.

### (TC. 3) Use the PET-Server
Trainers and Chiefs have to utilize the ``!petserver`` for trainings if at a PB facility.

### (TC. 4) Use UCT as training time
Trainers should use UTC to schedule trainings. However, using EST as an extra display time is also allowed.

Trainers are exempt from AM. 1 and AM. 6 during trainings. They are also exempt from AM. 7, 

## Managers

### (MS. 1) Host training for anyone
Managers are allowed to host trainings for all teams.

### (MS. 2) Exeption to 1h rule
Managers also need to follow the 1 Hour Training Policy, except they can override an SD PBST training. PBST is exempt from this rule. Only applies to PET.

## Medical Team Combat Medic Sub-Division

### (MT.4) PVP Heal
This is a Exemption to **MT.3**
You are allowed to combat and heal during combat as a Combat Medic. 

### (MT.4A) BE CAREFULL
Must be done carefully, don’t heal the enemy team.