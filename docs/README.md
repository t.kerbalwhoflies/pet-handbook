---
home: true
heroImage: /PBST-Logo.png
actionText: Select Division →
actionLink: /pet/
footer: "Last Modification of Rules: 30/11/2019"
forceTheme: green
---
::: warning NOTE:
The handbook was originally made by Dannycrew106. [This handbook can be found here](https://devforum.roblox.com/t/pinewood-emergency-team-handbook/507807)
:::

::: danger WARNING!
Please read through all of the rules very carefully. Any changes to the rules will be made clear. The rules have been categorized into who they apply to, however some rules may apply to more than one group of PET Members, in which case it will be made clear. Punishments for breaking a rule depend on what rule and the severity. Punishments can range from a simple warning, demotion, or even blacklist from PET.
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>
